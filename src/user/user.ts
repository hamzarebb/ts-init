export abstract class User
{
    protected username: string
    protected userpassword: string

    abstract store(username: string, userpassword: string): void
}