import { COLORS } from "../enum/color.enum";

export interface Paintable
{
    paint(color: COLORS): string
}