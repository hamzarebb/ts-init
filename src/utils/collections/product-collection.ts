import { Collection } from "./collection"
import { Product } from "../../products/product"

export class ProductCollection extends Collection<Product>
{
    add(object: Product): Collection<Product> 
    {
        this.collection.push(object)
        return this
    }
}