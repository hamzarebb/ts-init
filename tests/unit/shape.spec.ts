import { Rectangle } from "../../src/shapes/rectangle"
import { Shape } from "../../src/shapes/shape"
import { COLORS } from "../../src/utils/enum/color.enum"

describe(`Shapes`, () => 
{
    let shape: Rectangle

    beforeEach(() => 
    {
        shape = new Rectangle()
        shape.length = 10
        shape.width = 5
    })

    it(`should say 'Je peins en Vert'`, () => 
    {
        const paint = shape.paint(COLORS.GREEN)
        expect(paint).toBe('Je peins en Vert')
    })
})