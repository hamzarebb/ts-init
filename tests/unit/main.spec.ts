import { ascNumberSorter } from "../../src/utils/lib"

describe("main", () => 
{
    it("should return 3 if op1 equals 8 and op2 equals 5", () => 
    {
        expect(ascNumberSorter(8,5)).toEqual(3)
    })

    it('should have 2 as first item in an array after sorting', () => 
    {
        const array: Array<number> = [12, 5, 3, 8, 2]
        array.sort(ascNumberSorter)
        expect(array[0]).toEqual(2)
    })
})