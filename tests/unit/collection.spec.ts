import { Product } from "../../src/products/product"
import { ProductCollection } from "../../src/utils/collections/product-collection"

describe('Collection', () => 
{
    it('should have 1 product', () => 
    {
        const product = new Product()
        product.id = 'id'
        product.label = 'Product'
        product.stock = 5

        const collection: ProductCollection = new ProductCollection()
        collection.add(product)
        expect(collection.collection.length).toEqual(1)

        collection.addObject(product)
        expect(collection.collection.length).toEqual(2)
    })
})